// This contains the main page for Doxygen-documentation
/**
 * @mainpage Stack
 * This assignment was about creating and implementing a stack which was to pass a given set of tests.
 *
 * @ref about_the_authors <br>
 *
 * @section stack Stack
 * A stack is a container of data adhering to the LIFO-rules. Last in, first out.
 * Data is pushed on top of the stack and then popped off from the top down.
 *
 *
 * @section implementation The Implementation
 * The stack was implemented as a linked list. A secondary class was made which is used to create a node object. Every node has a data member
 * to store the stack data which will be of template specified data type. The node also contains a pointer which is set to point at another
 * node. This is the linking principle. Every new node put on the stack is initiated with a pointer pointing at the node on the stack directly
 * below it. This was the actual stack object only needs to hold a reference to the top node on the stack. Every node under it is connected with
 * a pointer link. When a value is popped off the stack, the top pointer is reset to point at the node second in line. The value inside
 * the popped node is retrieved to be returned to the user before the memory the node was occupying is explicitly deleted.
 *
 */

/**
* @page about_the_authors About the Authors
*
* @section author Test developer
* @subsection Author1 Christoffer Fink
* Christoffer is a teacher att Mitt Universitetet and he developed the tests used in this project to outline the requirements for the stack-implementation.
*
* <br/>
* @section author2 Code developer for the stack
* @subsection Author2 Eva Thilderkvist
* I'm a 28 year old software engineering student at the Mid Sweden University and I am doing my studies through the off-campus program.
* Parallel with my studies I have been travelling and working in the horse racing industry all over the world for the last ten years.
* The interest for programming was initially sparked through mathematics, analysis and problem solving relating to sports gambling.
*
*/

/**
 * @file stack.h
 * @author Eva Thilderkvist
 * @date November 2017
 * @version 0.1
*/

#ifndef Stack_H
#define Stack_H

#include<memory>
#include "memstat.hpp"

using namespace std;


/**
 * @class Node
 * @brief Class for creating an object holding a value of specified type and a pointer with reference to another node.
 * @details To be used on conjunction with a custom made container class. Since the node contains a pointer to another node
 * it can be used to create linked lists of different sorts. The kind of data stored in the node can be decided at declaration
 * as the class uses a template for it's internal data type.
 *
 * @test Is correct values returned?
*/
template<typename T>
class Node{
private:
    Node* prevNode;     /**< A pointer to another node of the same type*/
    T value;            /**< The data stored in the node*/

public:
    /**
     * @brief Constructor
     * @details Creates a new node with the help of the two parameters which will be set as the node data members. The type of
     * data wished to be stored in the stack is specified to the constructor.
     * @param aPrev Pointer to another node just to link this node.
     * @param aVal A value of node data type to be stored in the node.
     * @test Is the node created as specified by the parameters?
    */
    Node(Node* aPrev, T aVal) : prevNode(aPrev), value(aVal){};

    /**
     * @brief Destructor
     * @details Executes no additional commands.
     */
    ~Node(){};

    /**
     * @brief Returns the inner value of the node.
     * @details A get-method which retrieves the stored data value which will be of node-specified type.
     * @return A value of the type specified by the node.
     * @test Does the method return the data value passed at creation to the constructor?
     */
    T getValue(){return value;};

    /**
     * @brief Returns a pointer to the linked node.
     * @details Returns a pointer to the node which was linked to this node at construction.
     * @return A node of specified data type.
     * @test Does the method return a valid pointer?
     * @test Does the method return a nullptr if the previous node is not set?
     */
    Node* getPrevNode(){return prevNode;};
};

/**
 * @class Stack
 * @brief Container functioning as a stack.
 * @details A container made out of linked nodes. The container follow the rules of a stack which are specified as last in, first out.
 * The stack contains a pointer to the node currently on the top of the stack. Since every node added to the stack contains a reference
 * to the node under it all nodes of the stack can be accessed from the top down.
 * @test Does the stack-class follow the generally accepted rules of a stack?
 * @test Does the stack deallocate memory correctly?
 * @test Is all data passed to the stack able to be retrieved?
 * @test Is the data retrieved with it's integrity and in the right order?
 * @test Does an empty stack prevent the attempt of data retrieval?
*/
template<typename T>
class Stack{
private:
    Node<T>* top = nullptr;     /**< Pointer to the top of the stack*/
    size_t stackSize = 0;       /**< Counter keeping track of amount of nodes in the stack*/

public:
    /**
     * @brief Constructor
     * @details Creates a new stack of specified data type.
     * Ex: @code Stack<int>(); @endcode
     * @test Is a correct empty stack set up?
     */
    Stack(){};

    /**
     * @brief Destructor
     * @details Rolls back the stack by calling the pop method until empty. This deallocates the memory as the pop method executes delete
     * on every popped pointer.
     * @test Is there any memory leaks?
     */
    ~Stack();

    /**
     * @brief Checks if there is anything stored on the stack.
     * @details Uses the size data member to determine if the stack is empty or not.
     * @return A boolean value indicating if the stack is empty.
     * @test Does the return value correctly reflect the state of the stack?
     */
    bool empty(){return stackSize == 0;};

    /**
     * @brief Returns the current size of the stack.
     * @details Returns the value of the size data member. This data member is updated every time there is a pop or push.
     * @return A size value reflecting number of nodes on the stack.
     * @test Is the size value correct?
     * @test Does the size member update correctly?
     */
    size_t size(){ return stackSize;};

    /**
     * @brief Pops the top of the stack and returns the value inside this node.
     * @details First checks if the stack is empty. Then uses a temporary pointer to store the top node while it assigns the
     * top pointer to point to the value second in line. After the reassignment the previous top node's memory is deallocated and
     * the value stored in the node returned. The size counter is decremented.
     * @return The value of the top node
     * @test Is the stack updated correctly
     * @test Is correct value returned?
     * @test Is the memory of popped node deallocated properly?
     */
    T pop();

    /**
     * @brief Pushes a value on top of the stack.
     * @details Creates a new node to store the new value and a pointer to the previous stack top. Then assigns the top
     * pointer to point at the new node and increments the size counter.
     * @param addVal A value to be put on top of the stack.
     * @test Is the new value on top?
     * @test is the size updated accordingly?
     * @test It the integrity of the stack intact?
     */
    void push(T addVal);
};

template<typename T>
T Stack<T>::pop(){
    //First check if the stack is empty
    if (empty()) {
        throw std::range_error{"Can't pop empty stack"};
    }

    //Pointer to temporarely hold the node to be popped
    Node<T>* temp = top;
    top = temp->getPrevNode();

    //decrease the stack size
    --stackSize;

    //Extract the value before deleting the node and it's memory
    T val = temp->getValue();
    delete temp;

    return val;
}

template<typename T>
void Stack<T>::push(T addVal){
    //Create new node with link to old top and assign it the top position
    top = new Node<T>(top,addVal);

    //increase stack size
    ++stackSize;
}

template<typename T>
Stack<T>::~Stack() {
    //Keep popping off values as long as the stack is not empty
    while (stackSize > 0){
        pop();
    }
}

#endif



